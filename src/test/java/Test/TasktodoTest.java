package Test;

import static org.junit.Assert.*;

public class TasktodoTest {
    @Test
    public void testGetTask(){
        Task task = new Task();
        int id = 5;
        String name = "Belajar TDD",status = "[NOT DONE]";
        task.setId(id);
        task.setName(name);
        task.setStatus(status);
        String expectedResult = "5. Belajar TDD [NOT DONE]";
        assertEquals(expectedResult,task.getTask());
    }
    @Test
    public void testGetTaskOnNullReferences(){
        Task task = new Task();
        String expectedResult = "null references";
        assertEquals(expectedResult,task.getTask());
    }
    @Test
    public void testInsertStatusOnFalseOption(){
        Task task = new Task();
        task.setStatus("False Option");
        assertEquals("[]",task.getStatus());
    }

}