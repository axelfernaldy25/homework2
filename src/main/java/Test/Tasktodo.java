package Test;

import java.util.ArrayList;
import java.util.List;

public class Tasktodo {
    private List<Task> taskList = new ArrayList<Task>();
    public Tasktodo() {
    }

    public void insertTask(int id, String name, String status) {
        Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setStatus(status);
        taskList.add(task);
    }

    public Task getTask(int index) {
        return taskList.get(index);
    }

    public String getTodoList() {
        String todolist="";
        for(int i=0 ; i < taskList.size() ; i++){
            todolist+=taskList.get(i).getTask();
            if(i+1!=taskList.size()) {
                todolist += "\n";
            }
        }
        return todolist;
    }
}
