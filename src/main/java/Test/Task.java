package Test;

public class Task {
    private String name="";
    private String status="";
    private int id=0;

    public Task(){

    }
    public String getStatus(){
        return status;
    }
    public void setStatus(String status){
        this.status = status;
        if (status!="[DONE]"&&status!="[NOT DONE]"){
            this.status ="[]";
        }
    }
    public String getName(){
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTask(){
        if(this.id!=0&&this.name.length()!=0&&this.status.length()!=0){
            return this.id+". "+this.name+" "+this.status;
        }
        return "null references";
    }

    }


